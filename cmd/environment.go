package cmd

import (
	"github.com/spf13/viper"
	"gitlab.com/loranna/bootscripts/generate"
)

func init() {
	rootCmd.AddCommand(generate.Environment(viper.GetViper()))
}
