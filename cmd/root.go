package cmd

import (
	"math/rand"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var checkbroker, checkenvironment, checkloriot bool

func init() {
	rootCmd.PersistentFlags().BoolVar(&checkbroker, "checkbroker", true, "should bootscript check broker availability")
	viper.BindPFlag("checkbroker", rootCmd.PersistentFlags().Lookup("checkbroker"))
	viper.BindEnv("checkbroker", "CHECK_BROKER")
	rootCmd.PersistentFlags().BoolVar(&checkenvironment, "checkenvironment", true, "should bootscript check environment availability")
	viper.BindPFlag("checkenvironment", rootCmd.PersistentFlags().Lookup("checkenvironment"))
	viper.BindEnv("checkenvironment", "CHECK_ENVIRONMENT")
	rootCmd.PersistentFlags().BoolVar(&checkloriot, "checkloriot", true, "should bootscript check environment availability")
	viper.BindPFlag("checkloriot", rootCmd.PersistentFlags().Lookup("checkloriot"))
	viper.BindEnv("checkloriot", "CHECK_LORIOT")

}

var rootCmd = &cobra.Command{
	Use:           "bootscripts",
	Short:         "boot checks for loriot resource and generates if needed",
	SilenceErrors: false,
	SilenceUsage:  true,
}

func Execute() {
	rand.Seed(time.Now().UnixNano())
	if err := rootCmd.Execute(); err != nil {
		logrus.Fatal(err)
	}
}
