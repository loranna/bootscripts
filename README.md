# Bootscripts

Bootscript generates config files for various Loranna components.

## Available commands

See [available_commands.md](available_commands.md)

## bash completion usage

- make sure you have `bash-completion` package
- copy `bash.sh` and rename it to `vdevice` to bash-completion directory
- make sure functionality is enabled in `/etc/bash.bashrc` or `~/.bashrc`

## Environment variables

Environment variables are bound to flags

### Common

|flag|environment variable|
|---|---|
|checkbroker|CHECK_BROKER|
|checkenvironment|CHECK_ENVIRONMENT|
|devupchannelname|DEVUPCHANNELNAME|
|gwdnchannelname|GWDNCHANNELNAME|
|latitude|LATITUDE|
|longitude|LONGITUDE|
|loriotapikey|LORIOT_API_KEY|
|loriotserverurl|LORIOT_SERVER_URL|
|rabbitmqpassword|RABBITMQ_PASSWORD|
|rabbitmqport|RABBITMQ_PORT|
|rabbitmqserverurl|RABBITMQ_SERVER_URL|
|rabbitmqusername|RABITMQ_USERNAME|
|registermethod|REGISTER_METHOD|

### Device

|flag|environment variable|
|---|---|
|adr|ADR|
|appeui|APPEUI|
|appkey|APPKEY|
|appskey|APPSKEY|
|counterdown|COUNTER_DOWN|
|counterup|COUNTER_UP|
|devaddr|DEVADDR|
|deveui|DEVEUI|
|joinmethod|JOIN_METHOD|
|nwkskey|NWKSKEY|
|program|PROGRAM|
|title|TITLE|
|uplinkconfirmed|UPLINK_CONFIRMED|
|uplinkinterval|UPLINK_INTERVAL|
|uplinkpayload|UPLINK_PAYLOAD|
|uplinkport|UPLINK_PORT|

### Gateway

|flag|environment variable|
|---|---|
|interfacename|INTERFACE_NAME|
|macaddress|MAC_ADDRESS|

### Appout

It does not have extra environment variables
