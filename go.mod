module gitlab.com/loranna/bootscripts

go 1.13

require (
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/cobra v0.0.7
	github.com/spf13/viper v1.6.3
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	gitlab.com/loranna/loranna v0.0.7
	gitlab.com/loriot/api v0.2.27
	gopkg.in/yaml.v2 v2.2.8
)
