package generate

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	conf "gitlab.com/loranna/loranna/config"
	"gopkg.in/yaml.v2"
)

var loriotserverurl, loriotapikey string

func Appout(viper *viper.Viper) *cobra.Command {
	aconfig := conf.AppoutConfig{}
	appoutCmd := &cobra.Command{
		Use:   "appout",
		Short: "prepare config for appout main process",
		Example: "bootscripts appout --loriotserverurl aa --loriotapikey bb",
		RunE: func(cmd *cobra.Command, args []string) error {
			url := viper.GetString("loriotserverurl")
			if len(url) == 0 {
				return fmt.Errorf("loriot server url is not set")
			}
			apikey := viper.GetString("loriotapikey")
			if len(apikey) == 0 {
				return fmt.Errorf("loriot server api key is not set")
			}
			aconfig.LoriotAPIKey = apikey
			aconfig.LoriotServerURL = url
			contents, err := yaml.Marshal(aconfig)
			if err != nil {
				return err
			}
			err = conf.SaveToFile("config.yml", contents)
			if err != nil {
				return err
			}
			return nil
		},
	}
	appoutCmd.Flags().StringVar(&loriotserverurl, "loriotserverurl", "", "url of LORIOT server")
	viper.BindPFlag("loriotserverurl", appoutCmd.Flags().Lookup("loriotserverurl"))
	viper.BindEnv("loriotserverurl", "LORIOT_SERVER_URL")
	appoutCmd.Flags().StringVar(&loriotapikey, "loriotapikey", "", "api key of LORIOT server")
	viper.BindPFlag("loriotapikey", appoutCmd.Flags().Lookup("loriotapikey"))
	viper.BindEnv("loriotapikey", "LORIOT_API_KEY")
	return appoutCmd
}
