package generate

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	conf "gitlab.com/loranna/loranna/config"
	"gopkg.in/yaml.v2"
)

var rabbitmqserverurl, rabbitmqusername, rabbitmqpassword, devupchannelname, gwdnchannelname string
var rabbitmqport, environmentserverport uint

func Environment(viper *viper.Viper) *cobra.Command {
	econfig := conf.EnvironmentConfig{}
	environmentCmd := &cobra.Command{
		Use:   "environment",
		Short: "checks for environment and creates one if needed",
		RunE: func(cmd *cobra.Command, args []string) error {
			econfig.Devupchannelname = viper.GetString("devupchannelname")
			econfig.Gwdnchannelname = viper.GetString("gwdnchannelname")
			econfig.RabbitMQPort = viper.GetInt("rabbitmqport")
			econfig.RabbitMQPassword = viper.GetString("rabbitmqpassword")
			econfig.RabbitMQUsername = viper.GetString("rabbitmqusername")
			econfig.RabbitMQUrl = viper.GetString("rabbitmqserverurl")
			econfig.Serverport = viper.GetUint("environmentserverport")
			contents, err := yaml.Marshal(econfig)
			if err != nil {
				return err
			}
			err = conf.SaveToFile("config.yml", contents)
			if err != nil {
				return err
			}
			return nil
		},
	}
	environmentCmd.Flags().StringVar(&rabbitmqserverurl, "rabbitmqserverurl", "rabbitmq", "rabbitmqserverurl")
	viper.BindPFlag("rabbitmqserverurl", environmentCmd.Flags().Lookup("rabbitmqserverurl"))
	viper.BindEnv("rabbitmqserverurl", "RABBITMQ_SERVER_URL")
	environmentCmd.Flags().StringVar(&rabbitmqusername, "rabbitmqusername", "guest", "rabbitmqusername")
	viper.BindPFlag("rabbitmqusername", environmentCmd.Flags().Lookup("rabbitmqusername"))
	viper.BindEnv("rabbitmqusername", "RABITMQ_USERNAME")
	environmentCmd.Flags().StringVar(&rabbitmqpassword, "rabbitmqpassword", "guest", "rabbitmqpassword")
	viper.BindPFlag("rabbitmqpassword", environmentCmd.Flags().Lookup("rabbitmqpassword"))
	viper.BindEnv("rabbitmqpassword", "RABBITMQ_PASSWORD")
	environmentCmd.Flags().UintVar(&rabbitmqport, "rabbitmqport", 5672, "rabbitmqport")
	viper.BindPFlag("rabbitmqport", environmentCmd.Flags().Lookup("rabbitmqport"))
	viper.BindEnv("rabbitmqport", "RABBITMQ_PORT")
	environmentCmd.Flags().StringVar(&devupchannelname, "devupchannelname", "uplinktoenvironment", "devupchannelname")
	viper.BindPFlag("devupchannelname", environmentCmd.Flags().Lookup("devupchannelname"))
	viper.BindEnv("devupchannelname", "DEVUPCHANNELNAME")
	environmentCmd.Flags().StringVar(&gwdnchannelname, "gwdnchannelname", "downlinktoenvironment", "gwdnchannelname")
	viper.BindPFlag("gwdnchannelname", environmentCmd.Flags().Lookup("gwdnchannelname"))
	viper.BindEnv("gwdnchannelname", "GWDNCHANNELNAME")
	environmentCmd.Flags().UintVar(&environmentserverport, "environmentserverport", 36699, "environmentserverport")
	viper.BindPFlag("environmentserverport", environmentCmd.Flags().Lookup("environmentserverport"))
	viper.BindEnv("environmentserverport", "ENVIRONMENT_SERVER_PORT")
	return environmentCmd
}
