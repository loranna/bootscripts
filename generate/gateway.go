package generate

import (
	"bufio"
	"fmt"
	"net"
	"net/http"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	conf "gitlab.com/loranna/loranna/config"
	"gitlab.com/loriot/api/core"
	"gopkg.in/yaml.v2"
)

var macaddress, interfacename, duplexity string

func Gateway(viper *viper.Viper) *cobra.Command {
	gconfig := conf.GatewayConfig{}
	gatewayCmd := &cobra.Command{
		Use:   "gateway",
		Short: "checks for gateway and creates one if needed",
		RunE: func(cmd *cobra.Command, args []string) error {
			gconfig.RabbitMQUrl = viper.GetString("rabbitmqserverurl")
			gconfig.RabbitMQUsername = viper.GetString("rabbitmqusername")
			gconfig.RabbitMQPassword = viper.GetString("rabbitmqpassword")
			gconfig.RabbitMQPort = viper.GetInt("rabbitmqport")
			gconfig.Devupchannelname = viper.GetString("devupchannelname")
			gconfig.Gwdnchannelname = viper.GetString("gwdnchannelname")
			gconfig.Latitude = viper.GetFloat64("latitude")
			gconfig.Longitude = viper.GetFloat64("longitude")
			gconfig.EnvironmentServer = viper.GetString("environmentserver")
			gconfig.EnvironmentPort = viper.GetUint("environmentserverport")
			gconfig.Duplexity = viper.GetString("duplexity")
			gconfig.LoriotServerURL = viper.GetString("loriotserverurl")
			if len(gconfig.LoriotServerURL) == 0 {
				return fmt.Errorf("loriot server url is not set")
			}
			mac := viper.GetString("macaddress")
			ifacename := viper.GetString("interfacename")
			if len(mac) > 0 {
				_, err := net.ParseMAC(mac)
				if err != nil {
					return err
				}
			}
			if len(mac) == 0 {
				if len(ifacename) == 0 {
					iname, err := defaultroutename()
					if err != nil {
						return err
					}
					ifacename = iname
				}
				m, err := macaddrofinterface(ifacename)
				if err != nil {
					return err
				}
				mac = m
			} else {
				iname, err := interfaceofmacaddr(mac)
				if err != nil {
					return err
				}
				ifacename = iname
			}
			gconfig.MacAddress = mac
			gconfig.NetworkInterfaceName = ifacename
			logrus.Debugf("%s", ifacename)
			if viper.GetBool("checkbroker") {
				server := fmt.Sprintf("amqp://%s:%s@%s:%d/", gconfig.RabbitMQUsername,
					gconfig.RabbitMQPassword, gconfig.RabbitMQUrl, gconfig.RabbitMQPort)
				conn, err := amqp.Dial(server)
				if err != nil {
					return fmt.Errorf("connecting to %s failed: %w", server, err)
				}
				conn.Close()
			}
			if viper.GetBool("checkenvironment") {
				url := fmt.Sprintf("http://%s:%d/status", gconfig.EnvironmentServer, gconfig.EnvironmentPort)
				response, err := http.Get(url)
				if err != nil {
					return err
				}
				if response.StatusCode != 200 {
					return fmt.Errorf("environment status request returned %s", response.Status)
				}
			}

			if viper.GetBool("checkloriot") {
				url := viper.GetString("loriotserverurl")
				if len(url) == 0 {
					return fmt.Errorf("loriot server url is not set")
				}
				apikey := viper.GetString("loriotapikey")
				if len(apikey) == 0 {
					return fmt.Errorf("loriot server api key is not set")
				}
				client := core.NewClient(url, apikey)
				var nwks []core.Network
				switch rm := viper.GetString("registermethod"); rm {
				case "auto":
					var err error
					nwks, err = client.NetworkList()
					if err != nil {
						return err
					}
				case "off":
				}

				gwregistered := false
				var network core.Network
				switch rm := viper.GetString("registermethod"); rm {
				case "auto":
					//lets check if gateway is accessible with the api key (registered)
					for _, nwk := range nwks {
						gws, err := client.GatewaysList(fmt.Sprintf("%08X", nwk.ID))
						if err != nil {
							return err
						}
						for _, gw := range gws {
							if strings.ToUpper(gw.MAC) == strings.ToUpper(mac) {
								network = nwk
								gwregistered = true
							}
						}
					}
					//lets check if we have a network with name loranna
					if network.ID == 0 {
						for _, nwk := range nwks {
							if strings.ToLower(nwk.Name) == "loranna" {
								network = nwk
							}
						}
					}
					// create network if we don't have it
					if network.ID == 0 {
						err := client.NetworkAdd("loranna")
						if err != nil {
							return err
						}
						nwks, err = client.NetworkList()
						if err != nil {
							return err
						}
						for _, nwk := range nwks {
							if strings.ToLower(nwk.Name) == "loranna" {
								network = nwk
							}
						}
					}
				case "off":
				}
				if !gwregistered {
					switch rm := viper.GetString("registermethod"); rm {
					case "auto":
						//add gateway to network named loranna
						err := client.GatewayAdd(fmt.Sprintf("%08X", network.ID), mac)
						if err != nil {
							return err
						}
					case "off":
					}
				}
			}

			contents, err := yaml.Marshal(gconfig)
			if err != nil {
				return err
			}
			err = conf.SaveToFile("config.yml", contents)
			if err != nil {
				return err
			}
			return nil
		},
	}

	gatewayCmd.Flags().StringVar(&macaddress, "macaddress", "", "macaddress of the gateway")
	viper.BindPFlag("macaddress", gatewayCmd.Flags().Lookup("macaddress"))
	viper.BindEnv("macaddress", "MAC_ADDRESS")
	gatewayCmd.Flags().StringVar(&interfacename, "interfacename", "", "interfacename of the gateway")
	viper.BindPFlag("interfacename", gatewayCmd.Flags().Lookup("interfacename"))
	viper.BindEnv("interfacename", "INTERFACE_NAME")
	gatewayCmd.Flags().StringVar(&duplexity, "duplexity", "fullduplex", "duplexity of the gateway")
	viper.BindPFlag("duplexity", gatewayCmd.Flags().Lookup("duplexity"))
	viper.BindEnv("duplexity", "DUPLEXITY")
	gatewayCmd.Flags().StringVar(&loriotserverurl, "loriotserverurl", "", "url of LORIOT server")
	viper.BindPFlag("loriotserverurl", gatewayCmd.Flags().Lookup("loriotserverurl"))
	viper.BindEnv("loriotserverurl", "LORIOT_SERVER_URL")
	gatewayCmd.Flags().StringVar(&loriotapikey, "loriotapikey", "", "api key of LORIOT server")
	viper.BindPFlag("loriotapikey", gatewayCmd.Flags().Lookup("loriotapikey"))
	viper.BindEnv("loriotapikey", "LORIOT_API_KEY")
	gatewayCmd.Flags().StringVar(&environmentserver, "environmentserver", "environment", "environmentserver of device")
	viper.BindPFlag("environmentserver", gatewayCmd.Flags().Lookup("environmentserver"))
	viper.BindEnv("environmentserver", "ENVIRONMENT_SERVER")
	gatewayCmd.Flags().UintVar(&environmentserverport, "environmentserverport", 36699, "environmentserverport")
	viper.BindPFlag("environmentserverport", gatewayCmd.Flags().Lookup("environmentserverport"))
	viper.BindEnv("environmentserverport", "ENVIRONMENT_SERVER_PORT")
	gatewayCmd.Flags().StringVar(&rabbitmqserverurl, "rabbitmqserverurl", "rabbitmq", "rabbitmqserverurl")
	viper.BindPFlag("rabbitmqserverurl", gatewayCmd.Flags().Lookup("rabbitmqserverurl"))
	viper.BindEnv("rabbitmqserverurl", "RABBITMQ_SERVER_URL")
	gatewayCmd.Flags().StringVar(&rabbitmqusername, "rabbitmqusername", "guest", "rabbitmqusername")
	viper.BindPFlag("rabbitmqusername", gatewayCmd.Flags().Lookup("rabbitmqusername"))
	viper.BindEnv("rabbitmqusername", "RABBITMQ_USERNAME")
	gatewayCmd.Flags().StringVar(&rabbitmqpassword, "rabbitmqpassword", "guest", "rabbitmqpassword")
	viper.BindPFlag("rabbitmqpassword", gatewayCmd.Flags().Lookup("rabbitmqpassword"))
	viper.BindEnv("rabbitmqpassword", "RABBITMQ_PASSWORD")
	gatewayCmd.Flags().UintVar(&rabbitmqport, "rabbitmqport", 5672, "rabbitmqport")
	viper.BindPFlag("rabbitmqport", gatewayCmd.Flags().Lookup("rabbitmqport"))
	viper.BindEnv("rabbitmqport", "RABBITMQ_PORT")
	gatewayCmd.Flags().StringVar(&gwdnchannelname, "gwdnchannelname", "downlinktoenvironment", "gwdnchannelname")
	viper.BindPFlag("gwdnchannelname", gatewayCmd.Flags().Lookup("gwdnchannelname"))
	viper.BindEnv("gwdnchannelname", "GWDNCHANNELNAME")
	return gatewayCmd
}

func defaultroutename() (string, error) {
	file, err := os.Open("/proc/net/route")
	if err != nil {
		return "", fmt.Errorf("WARNING, i rely on /proc/net/route, actual error %w", err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		tokens := strings.Split(scanner.Text(), "\t")
		if tokens[1] == "00000000" {
			// default route
			return tokens[0], nil
		}
	}
	return "", fmt.Errorf("could not find default route's name")
}

func macaddrofinterface(ifname string) (string, error) {
	interfaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, i := range interfaces {
		if i.Name == strings.TrimSpace(ifname) {
			return i.HardwareAddr.String(), nil
		}
	}
	return "", fmt.Errorf("could not find hwaddr for %s", ifname)
}

func interfaceofmacaddr(mac string) (string, error) {

	interfaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, i := range interfaces {
		if i.HardwareAddr.String() == strings.TrimSpace(mac) {
			return i.Name, nil
		}
	}
	return "", fmt.Errorf("could not find interfacename for %s", mac)
}
