package generate

import (
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	conf "gitlab.com/loranna/loranna/config"
	"gitlab.com/loriot/api/core"
	"gopkg.in/yaml.v2"
)

var deveui, joinmethod, title, appeui, appkey, appskey, nwkskey,
	devaddr, uplinkpayload, registermethod, environmentserver string
var counterup, counterdown, uplinkinterval, program, rejoininterval,
	deviceserverport, environmentport uint
var latitude, longitude float64
var uplinkport uint8
var uplinkconfirmed, adr bool

func Device(viper *viper.Viper) *cobra.Command {
	dconfig := conf.DeviceConfig{}
	deviceCmd := &cobra.Command{
		Use:     "device",
		Short:   "checks for device and creates one if needed",
		Example: "bootscripts device --loriotserverurl aa --loriotapikey bb --checkbroker=false --checkenvironment=false --checkloriot=false",
		RunE: func(cmd *cobra.Command, args []string) error {
			rnum1 := rand.Uint64()
			viper.SetDefault("appeui", fmt.Sprintf("%016X", rnum1))
			rnum2 := rand.Uint64()
			rnum3 := rand.Uint64()
			viper.SetDefault("appkey", fmt.Sprintf("%016X%016X", rnum2, rnum3))
			rnum2 = rand.Uint64()
			rnum3 = rand.Uint64()
			viper.SetDefault("appskey", fmt.Sprintf("%016X%016X", rnum2, rnum3))
			rnum2 = rand.Uint64()
			rnum3 = rand.Uint64()
			viper.SetDefault("nwkskey", fmt.Sprintf("%016X%016X", rnum2, rnum3))
			rnum4 := rand.Uint32()
			viper.SetDefault("devaddr", fmt.Sprintf("%08X", rnum4))
			dconfig.RabbitMQUrl = viper.GetString("rabbitmqserverurl")
			dconfig.RabbitMQUsername = viper.GetString("rabbitmqusername")
			dconfig.RabbitMQPassword = viper.GetString("rabbitmqpassword")
			dconfig.RabbitMQPort = viper.GetInt("rabbitmqport")
			dconfig.Devupchannelname = viper.GetString("devupchannelname")
			dconfig.Gwdnchannelname = viper.GetString("gwdnchannelname")
			dconfig.Latitude = viper.GetFloat64("latitude")
			dconfig.Longitude = viper.GetFloat64("longitude")
			dconfig.UplinkPort = uint8(viper.GetUint("uplinkport"))
			dconfig.UplinkPayload = viper.GetString("uplinkpayload")
			dconfig.UplinkConfirmed = viper.GetBool("uplinkconfirmed")
			dconfig.UplinkInterval = viper.GetUint("uplinkinterval")
			dconfig.DeviceProgram = viper.GetUint("program")
			dconfig.DeviceAdr = viper.GetBool("adr")
			dconfig.EnvironmentServer = viper.GetString("environmentserver")
			dconfig.EnvironmentPort = viper.GetUint("environmentserverport")
			dconfig.ServerPort = viper.GetInt("deviceserverport")
			dconfig.RejoinInterval = viper.GetUint("rejoininterval")
			url := viper.GetString("loriotserverurl")
			if len(url) == 0 {
				return fmt.Errorf("loriot server url is not set")
			}
			apikey := viper.GetString("loriotapikey")
			if len(apikey) == 0 {
				return fmt.Errorf("loriot server api key is not set")
			}
			if viper.GetBool("checkbroker") {
				server := fmt.Sprintf("amqp://%s:%s@%s:%d/", dconfig.RabbitMQUsername,
					dconfig.RabbitMQPassword, dconfig.RabbitMQUrl, dconfig.RabbitMQPort)
				conn, err := amqp.Dial(server)
				if err != nil {
					return err
				}
				conn.Close()
			}
			if viper.GetBool("checkenvironment") {
				url := fmt.Sprintf("http://%s:%d/status", dconfig.EnvironmentServer, dconfig.EnvironmentPort)
				response, err := http.Get(url)
				if err != nil {
					return err
				}
				if response.StatusCode != 200 {
					return fmt.Errorf("environment status request returned %s", response.Status)
				}
			}
			client := core.NewClient(url, apikey)
			var apps []core.Application
			if viper.GetBool("checkloriot") {
				switch rm := viper.GetString("registermethod"); rm {
				case "auto":
					var err error
					apps, err = client.ApplicationList()
					if err != nil {
						return err
					}
				case "off":
				}
			}

			var device core.Device
			var application core.Application
			if deveui := viper.GetString("deveui"); len(deveui) == 16 {
				dconfig.Deveui = deveui
			} else {
				rnum := rand.Uint64()
				dconfig.Deveui = fmt.Sprintf("7AAAA5%02X%02X%02X%02X%02X", byte(rnum), byte(rnum>>8), byte(rnum>>16), byte(rnum>>24), byte(rnum>>32))
			}

			if viper.GetBool("checkloriot") {
				switch rm := viper.GetString("registermethod"); rm {
				case "auto":
					//lets check if device is accessible with the api key (registered)
					for _, app := range apps {
						devs, err := client.DevicesList(fmt.Sprintf("%08X", app.ID))
						if err != nil {
							return err
						}
						for _, dev := range devs {
							if strings.ToUpper(dev.Deveui) == strings.ToUpper(dconfig.Deveui) {
								application = app
								device = dev
							}
						}
					}
					//lets check if we have an application with name loranna
					if application.ID == 0 {
						for _, app := range apps {
							if strings.ToLower(app.Name) == "loranna" {
								application = app
							}
						}
					}
					// create application if we don't have it
					if application.ID == 0 {
						err := client.ApplicationAdd("loranna")
						if err != nil {
							return err
						}
						apps, err = client.ApplicationList()
						if err != nil {
							return err
						}
						for _, app := range apps {
							if strings.ToLower(app.Name) == "loranna" {
								application = app
							}
						}
					}
				case "off":
				}
				if len(device.ID) == 0 {
					switch rm := viper.GetString("registermethod"); rm {
					case "auto":
						//increase device limit if needed
						for {
							if application.Devices >= application.DeviceLimit {
								quota, err := client.Usage()
								if err != nil {
									return err
								}
								if quota.Devices < quota.Devlimit {
									err := client.IncreaseDeviceCapacity(fmt.Sprintf("%08X", application.ID), 10)
									if err != nil {
										return err
									}
									apps, err := client.ApplicationList()
									if err != nil {
										return err
									}
									for _, app := range apps {
										if strings.ToLower(app.Name) == "loranna" {
											application = app
										}
									}
								} else {
									return fmt.Errorf("not enough quota: %d/%d", quota.Devices, quota.Devlimit)
								}
							} else {
								break
							}
						}
					case "off":
					}
					dconfig.DeviceJoinMethod = viper.GetString("joinmethod")
					dconfig.DeviceTitle = viper.GetString("title")
					switch dconfig.DeviceJoinMethod {
					default:
						return fmt.Errorf("unknown join method")
					case "OTAA":
						dconfig.Appeui = viper.GetString("appeui")
						dconfig.Appkey = viper.GetString("appkey")
						switch rm := viper.GetString("registermethod"); rm {
						case "auto":
							err := client.DeviceOtaaAdd(fmt.Sprintf("%08X", application.ID),
								dconfig.Deveui,
								dconfig.Appeui,
								dconfig.Appkey,
								dconfig.DeviceTitle)
							if err != nil {
								return err
							}
						case "off":
						}
					case "ABP":
						dconfig.Nwkskey = viper.GetString("nwkskey")
						dconfig.Appskey = viper.GetString("appskey")
						dconfig.CounterUp = viper.GetUint("counterup")
						dconfig.CounterDown = viper.GetUint("counterdown")
						dconfig.Devaddr = viper.GetString("devaddr")
						switch rm := viper.GetString("registermethod"); rm {
						case "auto":
							err := client.DeviceAbpAdd(fmt.Sprintf("%08X", application.ID),
								dconfig.Deveui,
								dconfig.Devaddr,
								dconfig.Nwkskey,
								dconfig.Appskey,
								dconfig.DeviceTitle,
								int(dconfig.CounterUp),
								int(dconfig.CounterDown))
							if err != nil {
								return err
							}
						case "off":
						}
					}
				} else {
					dev, err := client.DeviceDetails(fmt.Sprintf("%08X", application.ID), device.Deveui)
					if err != nil {
						return err
					}
					dconfig.DeviceJoinMethod = viper.GetString("joinmethod")
					dconfig.DeviceTitle = dev.Title
					switch dconfig.DeviceJoinMethod {
					default:
						return fmt.Errorf("unknown join method")
					case "OTAA":
						dconfig.Appeui = dev.Appeui
						dconfig.Appkey = dev.Appkey
					case "ABP":
						dconfig.Nwkskey = dev.Nwkskey
						dconfig.Appskey = dev.Appskey
						if dev.Seqno < 0 {
							dconfig.CounterUp = 0
						} else {
							dconfig.CounterUp = uint(dev.Seqno)
						}
						if dev.Seqdn < 0 {
							dconfig.CounterDown = 0
						} else {
							dconfig.CounterDown = uint(dev.Seqdn)
						}
						dconfig.Devaddr = dev.Devaddr
					}
				}
			}
			dir, err := os.Getwd()
			if err != nil {
				return err
			}
			filename := path.Join(dir, "config.yml")
			contents, err := yaml.Marshal(dconfig)
			f, err := os.Create(filename)
			if err != nil {
				return err
			}
			_, err = f.Write(contents)
			if err != nil {
				return err
			}
			return nil
		},
	}

	deviceCmd.Flags().StringVar(&deveui, "deveui", "", "deveui of device")
	viper.BindPFlag("deveui", deviceCmd.Flags().Lookup("deveui"))
	viper.BindEnv("deveui", "DEVEUI")
	deviceCmd.Flags().StringVar(&joinmethod, "joinmethod", "OTAA", "joinmethod of device")
	viper.BindPFlag("joinmethod", deviceCmd.Flags().Lookup("joinmethod"))
	viper.BindEnv("joinmethod", "JOIN_METHOD")
	deviceCmd.Flags().StringVar(&title, "title", "loranna", "title of device")
	viper.BindPFlag("title", deviceCmd.Flags().Lookup("title"))
	viper.BindEnv("title", "TITLE")
	deviceCmd.Flags().StringVar(&appeui, "appeui", "", "appeui of device")
	viper.BindPFlag("appeui", deviceCmd.Flags().Lookup("appeui"))
	viper.BindEnv("appeui", "APPEUI")
	deviceCmd.Flags().StringVar(&appkey, "appkey", "", "appkey of device")
	viper.BindPFlag("appkey", deviceCmd.Flags().Lookup("appkey"))
	viper.BindEnv("appkey", "APPKEY")
	deviceCmd.Flags().StringVar(&devaddr, "devaddr", "", "devaddr of device")
	viper.BindPFlag("devaddr", deviceCmd.Flags().Lookup("devaddr"))
	viper.BindEnv("devaddr", "DEVADDR")
	deviceCmd.Flags().StringVar(&appskey, "appskey", "", "appskey of device")
	viper.BindPFlag("appskey", deviceCmd.Flags().Lookup("appskey"))
	viper.BindEnv("appskey", "APPSKEY")
	deviceCmd.Flags().StringVar(&nwkskey, "nwkskey", "", "nwkskey of device")
	viper.BindPFlag("nwkskey", deviceCmd.Flags().Lookup("nwkskey"))
	viper.BindEnv("nwkskey", "NWKSKEY")
	deviceCmd.Flags().UintVar(&counterup, "counterup", 0, "counterup of device")
	viper.BindPFlag("counterup", deviceCmd.Flags().Lookup("counterup"))
	viper.BindEnv("counterup", "COUNTER_UP")
	deviceCmd.Flags().UintVar(&counterdown, "counterdown", 0, "counterdown of device")
	viper.BindPFlag("counterdown", deviceCmd.Flags().Lookup("counterdown"))
	viper.BindEnv("counterdown", "COUNTER_DOWN")

	deviceCmd.Flags().Uint8Var(&uplinkport, "uplinkport", 5, "uplinkport of device")
	viper.BindPFlag("uplinkport", deviceCmd.Flags().Lookup("uplinkport"))
	viper.BindEnv("uplinkport", "UPLINK_PORT")
	deviceCmd.Flags().StringVar(&uplinkpayload, "uplinkpayload", "0000", "uplinkpayload of device")
	viper.BindPFlag("uplinkpayload", deviceCmd.Flags().Lookup("uplinkpayload"))
	viper.BindEnv("uplinkpayload", "UPLINK_PAYLOAD")
	deviceCmd.Flags().BoolVar(&uplinkconfirmed, "uplinkconfirmed", false, "uplinkconfirmed of device")
	viper.BindPFlag("uplinkconfirmed", deviceCmd.Flags().Lookup("uplinkconfirmed"))
	viper.BindEnv("uplinkconfirmed", "UPLINK_CONFIRMED")
	deviceCmd.Flags().UintVar(&uplinkinterval, "uplinkinterval", 5, "uplinkinterval of device")
	viper.BindPFlag("uplinkinterval", deviceCmd.Flags().Lookup("uplinkinterval"))
	viper.BindEnv("uplinkinterval", "UPLINK_INTERVAL")
	deviceCmd.Flags().UintVar(&program, "program", 0, "program of device")
	viper.BindPFlag("program", deviceCmd.Flags().Lookup("program"))
	viper.BindEnv("program", "PROGRAM")
	deviceCmd.Flags().BoolVar(&adr, "adr", false, "adr of device")
	viper.BindPFlag("adr", deviceCmd.Flags().Lookup("adr"))
	viper.BindEnv("adr", "ADR")
	deviceCmd.Flags().UintVar(&deviceserverport, "deviceserverport", 80, "port of device")
	viper.BindPFlag("deviceserverport", deviceCmd.Flags().Lookup("deviceserverport"))
	viper.BindEnv("deviceserverport", "DEVICE_SERVER_PORT")
	deviceCmd.Flags().UintVar(&rejoininterval, "rejoininterval", 10, "rejoininterval of device")
	viper.BindPFlag("rejoininterval", deviceCmd.Flags().Lookup("rejoininterval"))
	viper.BindEnv("rejoininterval", "REJOIN_INTERVAL")
	deviceCmd.Flags().Float64Var(&longitude, "longitude", 19.078116, "longitude of device")
	viper.BindPFlag("longitude", deviceCmd.Flags().Lookup("longitude"))
	viper.BindEnv("longitude", "LONGITUDE")
	deviceCmd.Flags().Float64Var(&latitude, "latitude", 47.515113, "latitude of device")
	viper.BindPFlag("latitude", deviceCmd.Flags().Lookup("latitude"))
	viper.BindEnv("latitude", "LATITUDE")
	deviceCmd.Flags().StringVar(&registermethod, "registermethod", "auto", "registermethod of device")
	viper.BindPFlag("registermethod", deviceCmd.Flags().Lookup("registermethod"))
	viper.BindEnv("registermethod", "REGISTER_METHOD")
	deviceCmd.Flags().StringVar(&environmentserver, "environmentserver", "environment", "environmentserver of device")
	viper.BindPFlag("environmentserver", deviceCmd.Flags().Lookup("environmentserver"))
	viper.BindEnv("environmentserver", "ENVIRONMENT_SERVER")
	deviceCmd.Flags().UintVar(&environmentserverport, "environmentserverport", 36699, "environmentserverport")
	viper.BindPFlag("environmentserverport", deviceCmd.Flags().Lookup("environmentserverport"))
	viper.BindEnv("environmentserverport", "ENVIRONMENT_SERVER_PORT")
	deviceCmd.Flags().StringVar(&rabbitmqserverurl, "rabbitmqserverurl", "rabbitmq", "rabbitmqserverurl")
	viper.BindPFlag("rabbitmqserverurl", deviceCmd.Flags().Lookup("rabbitmqserverurl"))
	viper.BindEnv("rabbitmqserverurl", "RABBITMQ_SERVER_URL")
	deviceCmd.Flags().StringVar(&rabbitmqusername, "rabbitmqusername", "guest", "rabbitmqusername")
	viper.BindPFlag("rabbitmqusername", deviceCmd.Flags().Lookup("rabbitmqusername"))
	viper.BindEnv("rabbitmqusername", "RABITMQ_USERNAME")
	deviceCmd.Flags().StringVar(&rabbitmqpassword, "rabbitmqpassword", "guest", "rabbitmqpassword")
	viper.BindPFlag("rabbitmqpassword", deviceCmd.Flags().Lookup("rabbitmqpassword"))
	viper.BindEnv("rabbitmqpassword", "RABBITMQ_PASSWORD")
	deviceCmd.Flags().UintVar(&rabbitmqport, "rabbitmqport", 5672, "rabbitmqport")
	viper.BindPFlag("rabbitmqport", deviceCmd.Flags().Lookup("rabbitmqport"))
	viper.BindEnv("rabbitmqport", "RABBITMQ_PORT")
	deviceCmd.Flags().StringVar(&devupchannelname, "devupchannelname", "uplinktoenvironment", "devupchannelname")
	viper.BindPFlag("devupchannelname", deviceCmd.Flags().Lookup("devupchannelname"))
	viper.BindEnv("devupchannelname", "DEVUPCHANNELNAME")
	deviceCmd.Flags().StringVar(&gwdnchannelname, "gwdnchannelname", "downlinktoenvironment", "gwdnchannelname")
	viper.BindPFlag("gwdnchannelname", deviceCmd.Flags().Lookup("gwdnchannelname"))
	viper.BindEnv("gwdnchannelname", "GWDNCHANNELNAME")
	deviceCmd.Flags().StringVar(&loriotserverurl, "loriotserverurl", "", "url of LORIOT server")
	viper.BindPFlag("loriotserverurl", deviceCmd.Flags().Lookup("loriotserverurl"))
	viper.BindEnv("loriotserverurl", "LORIOT_SERVER_URL")
	deviceCmd.Flags().StringVar(&loriotapikey, "loriotapikey", "", "api key of LORIOT server")
	viper.BindPFlag("loriotapikey", deviceCmd.Flags().Lookup("loriotapikey"))
	viper.BindEnv("loriotapikey", "LORIOT_API_KEY")
	return deviceCmd
}
