# Available commands

* [bootscripts](docs/bootscripts.md)
* [bootscripts appout](docs/bootscripts_appout.md)
* [bootscripts device](docs/bootscripts_device.md)
* [bootscripts gateway](docs/bootscripts_gateway.md)
* [bootscripts version](docs/bootscripts_version.md)
