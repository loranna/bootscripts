package main

import (
	"math/rand"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/loranna/bootscripts/cmd"
)

func main() {
	logrus.SetLevel(logrus.TraceLevel)
	rand.Seed(time.Now().UnixNano())
	cmd.Execute()
}
